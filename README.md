# GraphQ
Version 0.1

 
### Introduction ###
GraphQ is a graph query processing system, built on top of GraphChi (https://github.com/GraphChi/graphchi-java).

GraphQ can answer analytical queries with form "find n entities with a given quantitative property" by doing computation on partial graph.



### Acknowledgements ###