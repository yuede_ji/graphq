package edu.uci.graphQ.abstraction;

/**
 * @author Kai Wang
 *
 * Created by Aug 20, 2014
 */
public class AbstractEdge {
	private int from, to;
	private int weight;
	
	public AbstractEdge(int from, int to, int weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
	
	public AbstractEdge(int from, int to) {
		this(from, to ,0);
	}
	
	public String toString() {
		return "<" + from + ", " + to + " : " + weight + ">";
	}
	
	public int getFrom() {
		return from;
	}
	
	public void setFrom(int from) {
		this.from = from;
	}
	
	public int getTo() {
		return to;
	}
	
	public void setTo(int to) {
		this.to = to;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
}
