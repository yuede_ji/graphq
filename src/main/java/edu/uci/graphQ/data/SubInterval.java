package edu.uci.graphQ.data;

/**
 * @author Kai Wang
 *
 * Created by Aug 21, 2014
 */
public class SubInterval {

    private int firstVertex;
    private int lastVertex;
    private int intervalId;
    private int subIntervalId;

    public SubInterval(int firstVertex, int lastVertex, int intervalId, int subIntervalId) {
        this.firstVertex = firstVertex;
        this.lastVertex = lastVertex;
        this.intervalId = intervalId;
        this.subIntervalId = subIntervalId;
    }

    public int getFirstVertex() {
        return firstVertex;
    }

    public void setFirstVertex(int firstVertex) {
        this.firstVertex = firstVertex;
    }

    public int getLastVertex() {
        return lastVertex;
    }

    public void setLastVertex(int lastVertex) {
        this.lastVertex = lastVertex;
    }
    
    public int getIntervalId() {
    	return intervalId;
    }
    
    public void setIntervalId(int intervalId) {
    	this.intervalId = intervalId;
    }
    
    public int getSubIntervalId() {
    	return subIntervalId;
    }
    
    public void setSubIntervalId(int subIntervalId) {
    	this.subIntervalId = subIntervalId;
    }

    public String toString() {
        return "SubInterval " + firstVertex + " -- " + lastVertex + "  Interval Id : " + intervalId + "  SubInterval Id : " + subIntervalId;
    }


}
