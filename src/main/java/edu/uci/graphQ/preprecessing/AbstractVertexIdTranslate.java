package edu.uci.graphQ.preprecessing;

import java.util.ArrayList;

import edu.cmu.graphchi.engine.VertexInterval;

/**
 * @author Kai Wang
 *
 * Created by Aug 24, 2014
 */
public class AbstractVertexIdTranslate {
	private int numShards;
	private int numSubPartition;
	private int intervalLength;
	private ArrayList<VertexInterval> intervals = null;
	
	public AbstractVertexIdTranslate(int numShards, int numSubPartition, ArrayList<VertexInterval> intervals, int intervalLength) {
		this.numShards = numShards;
		this.numSubPartition = numSubPartition;
		this.intervals = intervals;
		this.intervalLength = intervalLength;
	}
	
	//TODO: how to quickly translate
	/**
     * Translates concrete vertex id to abstract vertex id
     * @param concreteId
     * @return
     */
	public int forward(int concreteId) {
		
		int intervalIndex = concreteId / intervalLength;
		VertexInterval interval = intervals.get(intervalIndex);
		int firstVertex = interval.getFirstVertex();
		int subLen = intervalLength / numSubPartition + 1;
		int subIntervalIndex = (concreteId - firstVertex) / subLen;
		
		return intervalIndex * numSubPartition + subIntervalIndex;
	}
	
	//TODO: how to quickly translate
	/**
     * Translates abstract vertex id to concrete vertex id
     * @param abstractId
     * @return
     */
	public int backward(int abstractId) {
		return 0;
	}
}
