package edu.uci.graphQ;

/**
 * @author Kai Wang
 *
 * Created by Sep 25, 2014
 */
public class GraphQConfig {
	public static boolean IN_COMBINE_MODE = false;
	
	public static void setCombineMode() {
		IN_COMBINE_MODE = true;
	}
}
